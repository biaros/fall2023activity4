// Bianca Rossetti
package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void getX_returns4()
    {
        Vector3d vector = new Vector3d(4, 3, 2);
        assertEquals(4, vector.getX(), 0.001);
    }

    @Test
    public void getY_returns3()
    {
        Vector3d vector = new Vector3d(4, 3, 2);
        assertEquals(3, vector.getY(), 0.001);
    }

    @Test
    public void getZ_returns2()
    {
        Vector3d vector = new Vector3d(4, 3, 2);
        assertEquals(2, vector.getZ(), 0.001);
    }

    @Test
    public void magnitude_returns30()
    {
        Vector3d vector = new Vector3d(10, 20, 20);
        assertEquals(30, vector.magnitude(), 0.001);
    }

    @Test
    public void dotProduct_returns13()
    {
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vectorInput = new Vector3d(2, 3, 4);
        assertEquals(13, vector.dotProduct(vectorInput), 0.001);
    }

    @Test
    public void add_returnsAddedVector()
    {
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vectorInput = new Vector3d(2, 3, 4);
        Vector3d vectorSum = vector.add(vectorInput);
        assertEquals(3, vectorSum.getX(), 0.001);
        assertEquals(4, vectorSum.getY(), 0.001);
        assertEquals(6, vectorSum.getZ(), 0.001);
    } 
}
