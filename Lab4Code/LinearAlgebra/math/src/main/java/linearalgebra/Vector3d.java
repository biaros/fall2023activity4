// Bianca Rossetti
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX()
    {
        return(this.x);
    }

    public double getY()
    {
        return(this.y);
    }

    public double getZ()
    {
        return(this.z);
    }

    public double magnitude()
    {
        return(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
    }

    public double dotProduct(Vector3d vector)
    {
        return((this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ()));
    }

    public Vector3d add(Vector3d vector)
    {
        double newX = this.x + vector.getX();
        double newY = this.y + vector.getY();
        double newZ = this.z + vector.getZ();

        Vector3d addedVector = new Vector3d(newX, newY, newZ);
        return(addedVector);
    }
}
